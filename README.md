# Keller estimates of the eigenvalues in the gap of Dirac operators

This is the code + pictures of the article:

*Keller estimates of the eigenvalues in the gap of Dirac operators.*
Jean Dolbeault, David Gontier, Fabio Pizzichillo, Hanne Van Den Bosch.
submitted


Arxiv version: https://arxiv.org/abs/2210.03091

Author: David Gontier (2022)